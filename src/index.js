import React,{useState} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';

function App() {
  const [checked,setChecked]=useState(false);

  return (
    <div>
      <input type="checkbox" value={checked} onChange={() => setChecked((checked) => !checked)}>
      </input>
      <p>{checked? "checked":"not checked"}</p>
    </div>
  )
}

ReactDOM.render(
  <React.StrictMode>
    <App name="Mahes" />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
